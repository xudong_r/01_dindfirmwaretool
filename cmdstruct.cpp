#include "cmdstruct.h"
#include "crc.h"

CMDSTRUCT::CMDSTRUCT()
{
    this->Init();
}

void CMDSTRUCT::Init()
{
    this->CmdHeader1 = 0;
    this->CmdHeader2 = 0;
    this->CmdTotalLength = 0;
//    this->CmdInd = 0;
//    this->CmdDataLength = 0;
    this->CmdData.clear();
    this->CRCH = 0;
    this->CRCL = 0;
    this->RxFlag = 0;


}

void CMDSTRUCT::SetCmdHeader1(tdu1 Header1)
{
    this->CmdHeader1 = Header1;
    this->RxFlag++;
}

void CMDSTRUCT::SetCmdHeader2(tdu1 Header2)
{
    this->CmdHeader2 = Header2;
    this->RxFlag++;
}

void CMDSTRUCT::SetCmdTotalLength(tdu1 TotalLength)
{
    this->CmdTotalLength = TotalLength;
    this->RxFlag++;
    this->CmdData.clear();
}

void CMDSTRUCT::AddCmdData(tdu1 Data)
{
    this->CmdData.append(Data);
    if((tdu1)this->CmdData.count() == this->CmdTotalLength-2)
    {
        this->RxFlag++;
    }
}

void CMDSTRUCT::SetCmdCRCL(tdu1 CrcL)
{
    this->CRCL = CrcL;
    this->RxFlag++;

}

void CMDSTRUCT::SetCmdCRCH(tdu1 CrcH)
{
    this->CRCH = CrcH;
    this->RxFlag++;
}

void CMDSTRUCT::ClearRxFlag(void)
{
    this->RxFlag = 0;
}

tdu1 CMDSTRUCT::GetRxFlag()
{
    return this->RxFlag;
}

tdu1 CMDSTRUCT::GetDataLength()
{
    return this->CmdTotalLength;
}

QByteArray CMDSTRUCT::GetCmdData()
{
    return this->CmdData;
}

tdu1 CMDSTRUCT::CmdCRCVerify()
{
    QByteArray CRCBuffer;
    tdu2 CRCValue;

    CRCBuffer.clear();

    CRCBuffer.append(this->CmdHeader1);
    CRCBuffer.append(this->CmdHeader2);
    CRCBuffer.append(this->CmdTotalLength);
    if(!this->CmdData.isEmpty())
    {
        CRCBuffer.append(this->CmdData);
    }
    CRCBuffer.append(this->CRCL);
    CRCBuffer.append(this->CRCH);

    CRCValue = CRC::CRC_AheadX((tdu1*)CRCBuffer.data(),this->CmdTotalLength+3);

    if(CRCValue == 0){
        return 1;
    }else
    {
        return 0;
    }

}



