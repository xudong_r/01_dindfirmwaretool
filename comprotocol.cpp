#include "comprotocol.h"
#include "mainwidget.h"
#include "crc.h"
#include <QDebug>

COMPROTOCOL::COMPROTOCOL()
{
    this->RxCmd = new CMDSTRUCT();
    this->RxBytes = new QByteArray();
    this->RxBytes->clear();
    this->ComProtocolHeader1 = 0xEB;
    this->ComProtocolHeader2 = 0x90;

}

void COMPROTOCOL::COMRxData(QByteArray Bytes)
{
    tdu1 Temp;
    static tdbl lock = false;
    RxBytes->append(Bytes);
    do
    {
        Temp = RxBytes->at(0);
        qDebug()<<(QString::number(Temp,16));
        if(RxCmd->GetRxFlag() == 0)
        {
            if(Temp==this->ComProtocolHeader1)
            {
                //检测到首字节
                RxCmd->SetCmdHeader1(Temp);

            }else
            {
                RxCmd->Init();
            }

        }
        else if(RxCmd->GetRxFlag() == 1)
        {
            if(Temp == this->ComProtocolHeader2)
            {
                RxCmd->SetCmdHeader2(Temp);
            }else
            {
                RxCmd->Init();
            }
        }
        else if(RxCmd->GetRxFlag() == 2)
        {
            //收到全部长度
            RxCmd->SetCmdTotalLength(Temp);
        }
        else if(RxCmd->GetRxFlag()==3)
        {
            RxCmd->AddCmdData(Temp);
        }else if(RxCmd->GetRxFlag()==4)
        {
            RxCmd->SetCmdCRCL(Temp);
        }else if(RxCmd->GetRxFlag()==5)
        {
            RxCmd->SetCmdCRCH(Temp);
        }
        if(RxCmd->GetRxFlag()==6)
        {
            //接 收到完整数据，进行CRC校验

                //CRC校验
                if(RxCmd->CmdCRCVerify())
                {
                    //CRC校验成功,执行CMD
                    if(lock == false)
                    {
                        //CRC校验成功,执行CMD
                        lock = true;
                        this->COMRxProcess(RxCmd);
                        lock = false;
                    }
                }

            RxCmd->Init();
        }
        RxBytes->remove(0,1);
    }while(!RxBytes->isEmpty());
}

void COMPROTOCOL::COMRxProcess(CMDSTRUCT *RxCmd)
{
    qDebug() << "COMRxProcess start";
    tZDLinkData ZDdata;
    tdu1 dataIndex = 0;
    tdu1 remLength = RxCmd->GetDataLength()-2;

    ZDdata.ID = RxCmd->GetCmdData().at(dataIndex);
    ZDdata.Length = RxCmd->GetCmdData().at(dataIndex+1);
    ZDdata.Data = RxCmd->GetCmdData().mid(dataIndex+2, ZDdata.Length);
    qDebug() << "COMRxProcess end";
    emit COMRxCmd(&ZDdata);

    while(remLength > ZDdata.Length + 2)
    {
        remLength -= (ZDdata.Length + 2);
        dataIndex += ZDdata.Length + 2;
        ZDdata.ID = RxCmd->GetCmdData().at(dataIndex);
        ZDdata.Length = RxCmd->GetCmdData().at(dataIndex+1);
        ZDdata.Data = RxCmd->GetCmdData().mid(dataIndex+2, ZDdata.Length);
        emit COMRxCmd(&ZDdata);

    }
    RxCmd->Init();
}


QByteArray COMPROTOCOL::SerialSendData(tdu1 CmdID)
{
    tdu2 CRCValue;
    QByteArray TempByteArray;

    TempByteArray.clear();

    TempByteArray.append(ComProtocolHeader1);
    TempByteArray.append(ComProtocolHeader2);

    TempByteArray.append(0x04);
    TempByteArray.append(CmdID);
    TempByteArray.append((char)0x00);

    CRCValue = CRC::CRC_AheadX((tdu1*)TempByteArray.data(),5);
    TempByteArray.append((tdu1)(CRCValue&0xff));
    TempByteArray.append((tdu1)((CRCValue>>8)&0xff));

    return TempByteArray;
}

QByteArray COMPROTOCOL::SerialSendData(tdu1 CmdID, QByteArray Data, tdu1 DataLength)
{
    tdu2 CRCValue;
    QByteArray TempByteArray;

    TempByteArray.clear();

    TempByteArray.append(ComProtocolHeader1);
    TempByteArray.append(ComProtocolHeader2);

    TempByteArray.append(DataLength+4);
    TempByteArray.append(CmdID);
    TempByteArray.append(DataLength);
    if(DataLength)
    {
        TempByteArray.append(Data);
    }
    CRCValue = CRC::CRC_AheadX((tdu1*)TempByteArray.data(),(DataLength+5));
    TempByteArray.append((tdu1)(CRCValue&0xff));
    TempByteArray.append((tdu1)((CRCValue>>8)&0xff));

    return TempByteArray;
}





