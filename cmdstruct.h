#ifndef CMDSTRUCT_H
#define CMDSTRUCT_H

#include "typedef.h"
#include <QVector>
class CMDSTRUCT
{
public:
    CMDSTRUCT();
    void Init();
    void SetCmdHeader1(tdu1 Header1);
    void SetCmdHeader2(tdu1 Header2);
    void SetCmdTotalLength(tdu1 TotalLength);
    void AddCmdData(tdu1 Data);
    void SetCmdCRCL(tdu1 CrcL);
    void SetCmdCRCH(tdu1 CrcH);
    void ClearRxFlag(void);

    tdu1 GetRxFlag(void);
    tdu1 GetDataLength(void);
    QByteArray GetCmdData(void);
    tdu1 CmdCRCVerify(void);

private:
    tdu1 CmdHeader1;
    tdu1 CmdHeader2;
    tdu1 CmdTotalLength;
    QByteArray CmdData;
    tdu1 CRCL;
    tdu1 CRCH;


    //0: 接收HEADER1
    //1: 接收HEADER2
    //2: 接收TotalLength
    //3: 接收CmdInd
    //4：接收CmdDataLength
    //5: 接收CmdData
    //6: 接收CRCL
    //7： 接收CRCH
    tdu1 RxFlag;
};

#endif // CMDSTRUCT_H
