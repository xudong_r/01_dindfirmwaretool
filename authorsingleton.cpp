
#include "authorsingleton.h"
#include <QDir>
#include <QDebug>

AuthorSingleton::AuthorSingleton() :
    mpLibLicense(NULL),
    mpSoftLicense(NULL),
    mpFuncCreate(NULL),
    mpFuncRelease(NULL),
    mSlValue1(0),
    mSlValue2(0),
    mSlLevel(0),
    mCheckNum(0)
{
}

AuthorSingleton::~AuthorSingleton()
{
    if (mpLibLicense != NULL) {
        if (mpSoftLicense != NULL && mpFuncRelease != NULL) {
            mpFuncRelease(mpSoftLicense);
            mpSoftLicense = NULL;
            mpFuncRelease = NULL;
        }
        mpLibLicense->unload();
        delete mpLibLicense;
        mpLibLicense = NULL;
    }
}

AuthorSingleton& AuthorSingleton::instance()
{
    static AuthorSingleton instance;
    return instance;
}

bool AuthorSingleton::initLibrary()
{
#ifdef PRODUCT_RELEASE1
    if (mpLibLicense == NULL) {
        mpLibLicense = new QLibrary(dllPathname());
        if (!mpLibLicense->load()) {
            qDebug("Fail to load library!");
            delete mpLibLicense;
            mpLibLicense = NULL;
            return false;
        }
        mpFuncCreate = (FUNC_CREATEOBJECT)mpLibLicense->resolve("createObject");
        mpFuncRelease = (FUNC_RELEASEOBJECT)mpLibLicense->resolve("releseObject");
        if (mpFuncCreate == NULL || mpFuncRelease == NULL) {
            qDebug("Fail to resolve function!");
            mpLibLicense->unload();
            delete mpLibLicense;
            mpLibLicense = NULL;
            return false;
        }
        mpSoftLicense = mpFuncCreate(DEFAULT_KEY1, DEFAULT_KEY2);
        if (mpSoftLicense == NULL) {
            qDebug("Fail to create object!");
            mpLibLicense->unload();
            delete mpLibLicense;
            mpLibLicense = NULL;
            return false;
        }
        mCheckNum = 0;
    }
#endif
    return true;
}

QString AuthorSingleton::dllPathname()
{
    QString path = QDir::currentPath() + "/SoftwareLicense";
//#ifndef QT_NO_DEBUG
//    path.append('D');
//#endif

#ifdef Q_OS_WIN
    path.append(".dll");
#endif
    return path;
}

bool AuthorSingleton::checkLicense(bool deep)
{
#ifdef PRODUCT_RELEASE1
    if (mpSoftLicense == NULL) {
        if (!initLibrary()) {
            return false;
        }
    }

    ++mCheckNum;
    unsigned int value;
    if (deep) {
        value = mpSoftLicense->checkLicense();
        if (value == 0) {
            qDebug("License is wrong!");
            return false;
        }
        if (mCheckNum == 1) {
            mSlValue1 = value;
            mSlValue2 = mpSoftLicense->getValue();
            mSlLevel = mpSoftLicense->getLevel();
        }
        return true;
    } else {
        value = mpSoftLicense->checkSum();
    }
    qDebug("AuthorSingleton::checkLicense: - Value: %u, %d, %u, %u, calc: %u",
           value, mCheckNum, mSlValue1, mSlValue2, (mCheckNum ^ (mSlValue1 / DEFAULT_KEY1) ^ (mSlValue2 + DEFAULT_KEY2)));
    return ((mCheckNum ^ (mSlValue1 / DEFAULT_KEY1) ^ (mSlValue2 + DEFAULT_KEY2)) == value);
#else
    Q_UNUSED(deep);
    return true;
#endif
}

int AuthorSingleton::checkSum()
{
#ifdef PRODUCT_RELEASE1
    if (mpSoftLicense == NULL) {
        return 0;
    }
    unsigned int value = mpSoftLicense->checkSum();
    ++mCheckNum;
    bool ret = ((mCheckNum ^ (mSlValue1 / DEFAULT_KEY1) ^ (mSlValue2 + DEFAULT_KEY2)) == value);
    qDebug("AuthorSingleton::checkLicense: - Value: %u, %d, %u, %u, calc: %u, res = %d",
           value, mCheckNum, mSlValue1, mSlValue2, (mCheckNum ^ (mSlValue1 / DEFAULT_KEY1) ^ (mSlValue2 + DEFAULT_KEY2)), ret);
    return ret ? mCheckNum : 0;
#else
    return mCheckNum;
#endif
}
