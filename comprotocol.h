#ifndef COMPROTOCOL_H
#define COMPROTOCOL_H

#include <QObject>
#include <typedef.h>
#include "cmdstruct.h"

typedef struct
{
    tdu1 ID;
    tdu1 Length;
    QByteArray Data;
}tZDLinkData;



class COMPROTOCOL: public QObject
{
    Q_OBJECT
public:
    COMPROTOCOL();
    void COMRxData(QByteArray Bytes);
    void COMRxCmd(CMDSTRUCT* RxCmd);

    //串口发送数据缓存
    QByteArray SerialSendBuffer;
    QByteArray SerialSendData(tdu1 CmdID);
    QByteArray SerialSendData(tdu1 CmdID,QByteArray Data,tdu1 DataLength);
private:
    CMDSTRUCT* RxCmd;
    QByteArray* RxBytes;
    tdu1 ComProtocolHeader1;
    tdu1 ComProtocolHeader2;

    void COMRxProcess(CMDSTRUCT *RxCmd);

signals:
    void COMRxCmd(tZDLinkData *);


};

#endif // COMPROTOCOL_H
